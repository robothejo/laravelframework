<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuplierModel extends Model
{
    protected $table = 'suplier';
    public function hasManyProduct(){   //hasManyProduct ini namanya bebas 
        return $this->hasMany(BarangModel::class, 'id_suplier', 'id_suplier');     //hasMany ini nama fungsi laravel, id_suplier yang pertama foreign key yang ke dua primary key 
    }
}
