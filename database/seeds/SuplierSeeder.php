<?php

use Illuminate\Database\Seeder;

class SuplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('suplier')->insert(array(
            array(
                'id_suplier'   => 'Sup-001',
                'nama_suplier'  => 'PT. Tirta',
                'no_tlp'          => '02100013',
                'alamat'    => 'Ciamis',
               
            ),
            array(
                'id_suplier'   => 'Sup-002',
                'nama_suplier'  => 'PT. Sukses Mandiri',
                'no_tlp'          => '02133323',
                'alamat'    => 'Tasikmalaya',
            ),
        ));
    }
}
