<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SuplierSeeder::class); //posisi table master harus di atas baik di seeder maupun di migration
        $this->call(BarangSeeder::class);
        $this->call(UserSeeder::class);
    }
}
